#!/usr/bin/env python
################################################################################
# @Title: parse_configs.py
#
# @Author: Phil Smith
#
# @Date: Mon, 27-Mar-17 06:43AM
#
# @Project: Examples
#
# @Purpose: Parse .cfg files and generate headers.
#
#
################################################################################
import re
import os
import glob

def parse(file_="system1.cfg"):
    """
    Parse the given file and create a header file for it.
    """

    # Open the config file, read the contents and write the header file
    with open(file_, 'r') as input_file:

        # Get a base structure name
        base_name = file_.split(".")[-2]

        # Swap the suffix of the file for a header file
        output_name = base_name + ".h"

        # Start writing the output file
        output_file = open(output_name, 'w')

        output_file.write("#ifndef __{}_H__\n".format(base_name))
        output_file.write("#define __{}_H__\n".format(base_name))
        output_file.write("struct {}_t {{\n".format(base_name))
        # Look through each line
        for line in input_file:

            # Split the line into a known set of fields
            #  <name> : <count> : <type> : <comment>
            data = re.search("(\w+?)\s*:\s*(\d+?)\s*:\s*(\w+?)\s*:\s*(\w.*)", line.strip())

            # If the line conforms to the specified format
            if data:
                name = data.group(1)
                count = int(data.group(2))

                # This can be turned into a key/value pair so that C types don't
                # have to be used.
                type_ = data.group(3)

                comment = data.group(4)

                # Handle array variables
                if count > 1:
                    output_file.write( "    {} {}[{}] //{}\n".format(type_, name, count, comment))
                else:
                    output_file.write( "    {} {} //{}\n".format(type_, name, comment))

        output_file.write("};\n")
        output_file.write("#endif /* __{}_H__ */\n".format(base_name))


def get_files(dir_="./"):
    """
    Open the given directory and find all of the *.cfg files.
    """

    # Change to the directory containing the config files
    os.chdir(dir_)

    config_files = glob.glob("*.cfg")

    for each in config_files:
        parse(each)

if __name__ == '__main__':
    get_files(".")
